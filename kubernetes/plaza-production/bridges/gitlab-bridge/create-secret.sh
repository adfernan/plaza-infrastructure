#!/bin/sh

SECRET_NAME=plaza-gitlab-bridge
SECRET_NAMESPACE=plaza-production

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>/dev/null 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

read -p "Bridge endpoint: " BRIDGE_ENDPOINT

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" \
	--from-literal=bridge_endpoint="$BRIDGE_ENDPOINT" \
