#!/bin/sh

SECRET_NAME=plaza-telegram-bridge
SECRET_NAMESPACE=plaza-production

kubectl -n "${SECRET_NAMESPACE}" get secret "${SECRET_NAME}" 2>/dev/null 1>/dev/null

if [ $? -eq 0 ];then
    echo "Secret already exists"
    exit 0
fi

set -eu

read -p "Bridge endpoint: " BRIDGE_ENDPOINT
read -p "Telegram token: " TELEGRAM_TOKEN
read -p "Telegram bot name: " TELEGRAM_BOT_NAME
read -p "Maintainer telegram handle: " MAINTAINER_TELEGRAM_HANDLE

kubectl -n "${SECRET_NAMESPACE}" create secret generic "${SECRET_NAME}" \
	--from-literal=bridge_endpoint="$BRIDGE_ENDPOINT" \
	--from-literal=telegram_token="$TELEGRAM_TOKEN" \
	--from-literal=telegram_bot_name="$TELEGRAM_BOT_NAME" \
	--from-literal=maintainer_telegram_handle="$MAINTAINER_TELEGRAM_HANDLE"
