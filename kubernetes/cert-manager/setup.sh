#!/bin/sh

set -eu

kubectl create namespace cert-manager
kubectl apply -n cert-manager -f 00-crds.yaml
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation="true"

kubectl apply -n cert-manager -f letsencrypt.yaml

helm repo add jetstack https://charts.jetstack.io

helm fetch jetstack/cert-manager -d ../output/charts/

rm -Rf ../output/templates/cert-manager/
helm template -f values.yaml --name=entrypoint ../output/charts/cert-manager-v0.7.2.tgz --namespace cert-manager --output-dir ../output/templates/

kubectl apply -n cert-manager \
       -f ../output/templates/cert-manager/templates/rbac.yaml                             \
       -f ../output/templates/cert-manager/templates/serviceaccount.yaml                   \
       -f ../output/templates/cert-manager/templates/deployment.yaml                       \
       -f ../output/templates/cert-manager/charts/cainjector/templates/rbac.yaml           \
       -f ../output/templates/cert-manager/charts/cainjector/templates/serviceaccount.yaml \
       -f ../output/templates/cert-manager/charts/cainjector/templates/deployment.yaml
