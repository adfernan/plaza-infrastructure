#!/bin/sh

set -eu

git submodule update --init --recursive .

## Create base
kubectl apply -f plaza-production/namespaces.yaml          \
                                                           \
              -f plaza-operation/namespaces.yaml           \
              -f plaza-operation/helm-sa.yaml              \
              -f plaza-operation/helm-clusterrole.yaml     \
              -f plaza-operation/helm-clusterrolebinding.yaml

## Install ingress
mkdir -p output/charts || true
mkdir -p output/templates/|| true
helm fetch -d output/charts/ stable/nginx-ingress
helm template -f ingress/nginx-ingress-values.yaml --name=entrypoint  output/charts/nginx-ingress-1.6.0.tgz --namespace plaza-production --output-dir output/templates/
kubectl -n plaza-production apply -f output/templates/nginx-ingress/*

# Install cert manager
cd cert-manager
sh setup.sh
cd ..
