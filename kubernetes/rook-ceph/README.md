Steps
=====

See https://rook.github.io/docs/rook/v1.0/ceph-examples.html for the proper procedure.

Remember
========

* Remove `/var/lib/rook/` from the hosts if you're retrying the installation.
* You need an unused storage device (where the data is going to be stored). This device must be unpartitioned.
